- [Reference repo](https://github.com/antoniopapa/django-admin)
- [django rest api quickstart](https://www.django-rest-framework.org/tutorial/quickstart/)
- [Dockerfile](./Dockerfile)
- [docker-compose.yaml](./docker-compose.yaml)
- [requirements.txt](./requirements.txt)

## django project creation steps.
```bash
python -m venv env
env\Scripts\activate.ps1
pip install django
pip install djangorestframework
django-admin startproject tutorial .
```

vagrant steps:
```bash
vagrant up
vagrant ssh
cd /vagrant
```

Docker steps:
```bash
docker-compose up
docker-compose exec admin_api sh
# python manage.py startapp users
# python manage.py migrate
# python manage.py createsuperuser --email a@a.com
```

- [Django admin page](http://localhost:8000/admin)